# wordpress_simplon



Simplon is  a site about the students in formation at simplon with their presentations as posts. You can see the lists of all the students and create as well as login if you already have an account. 

## Notice
Php my admin and apache was used for this site, as such the get started section will have directions for php and apache. Adapt accordingly if you are using something else

## To get started
 - Install repository either as a zip or using the following command in your terminal `git@gitlab.com:erorua16/wordpress_simplon.git`
 - Create a database with your chosen name (ex: wordpress)
 - Go to the following directory `/etc/hosts` and add the following to the file:
```
  # Projet wordpress
  127.0.0.1   simplon
```    
 - Create a file `simplon.conf` in the following directory `/etc/apache2/sites-available/`
 - Edit the file with the following information
``` 
    <VirtualHost *:8000>

     ServerName simplon
     DocumentRoot (INSERT YOUR DIRECTORY PATH)

     <Directory (INSERT YOUR DIRECTORY PATH)>
            Options FollowSymLinks
            AllowOverride AllowOverride
            Require all granted
            </Directory>
     </VirtualHost>
```
 - In your terminal, run the following commands to activate the server: 
 * `sudo a2ensite simplon.conf`
 * `systemctl reload apache2`
 * `apachectl configtest`
 if the a`pachectl configtest` returns `syntax ok`, then the server is working
 - To access the site, go to the following link in your navigator `http://simplon`
